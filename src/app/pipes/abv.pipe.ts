import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abv'
})
export class AbvPipe implements PipeTransform {

  transform(value: number, ...args: any[]): string {
    // console.log('AbvPipe: ', value);
    if(args[0] === 'º') {
      return value + 'º';
    }

    if(value < 0 || value > 96) {  
      return value + '!!!!';
    }
    
    return value + '%';
  }

}
