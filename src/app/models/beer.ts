export class Beer {

    _name = '';
    _description = '';
    _image = '';
    _tagline = '';
    _firstBrewed = '';
    _abv = 0;


    constructor(data: any){
        this._name = data.name;
        this._description = data.description;
        this._image = data.image_url;
        this._tagline = data.tagline;
        this._firstBrewed = data.first_brewed;
        this._abv = data.abv;
    }

    get name() {
        return this._name;
    }

    get description() {
        return this._description;
    }

    get image() {
        return this._image;
    }

    get tagline() {
        return this._tagline;
    }

    get firstBrewed() {
        return this._firstBrewed;
    }

    get abv() {
        return this._abv;
    }

}