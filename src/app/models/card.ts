export class Card {

    private _category = '';
    private _type = '';
    private _difficulty = '';
    private _question = '';
    private _correctAnswer = '';
    private _answers: string[] = [];
    private _responded = false;
    private _rightAnswered = false;


    constructor(data?: any) {
        if (data !== undefined) {
            this._category = data.category;
            this._type = data.type;
            this._difficulty = data.difficulty;
            this._question = data.question;
            this._correctAnswer = data.correct_answer;
            this._answers = data.incorrect_answers;
            this._answers.push(this._correctAnswer);
        }
    }

    get category(): string {
        return this._category;
    }

    get type(): string {
        return this._type;
    }

    get difficulty(): string {
        return this._difficulty;
    }

    get question(): string {
        return this._question;
    }

    get correctAnswer(): string {
        return this._correctAnswer;
    }

    get answers(): string[] {
        return this._answers;
    }

    get isResponded(): boolean {
        return this._responded;
    }

    set isResponded(value: boolean) {
        this._responded = value;
    }

    get rightAnswered(): boolean {
        return this._rightAnswered;
    }

    set rightAnswered(value: boolean) {
        this._rightAnswered = value;
    }
}