export class Apod {

    // raw data
    private _title = '';
    private _date = '';
    private _explanation = '';
    private _url = '';
    private _hdurl = '';
    private _mediaType = '';
    private _serviceVersion = '';

    // processed data
    private _youtubeId = '';
    private _isVideo = false;
    private _isImage = false;

    constructor(data?: any) {
        if (data !== undefined) {
            this._title = data.title;
            this._date = data.date;
            this._explanation = data.explanation;
            this._url = data.url;
            this._hdurl = data.hdurl;
            this._mediaType = data.media_type;
            if (this._mediaType === 'video') {
                this._isVideo = true;
                this._isImage = false;
            }else if (this._mediaType === 'image') {
                this._isImage = true;
                this._isVideo = false;
            }

            this._serviceVersion = data.service_version;

            if (this._mediaType === 'video') {
                // split between embed/ and ? to get the youtube id
                this._youtubeId = this._url.split('embed/')[1];
                this._youtubeId = this._youtubeId.split('?')[0];
            }
        }
    }

    get title() {
        return this._title;
    }

    get date() {
        return this._date;
    }

    get explanation() {
        return this._explanation;
    }

    get url() {
        return this._url;
    }

    get hdurl() {
        return this._hdurl;
    }

    get mediaType() {
        return this._mediaType;
    }

    get serviceVersion() {
        return this._serviceVersion;
    }

    get youtubeId() {
        return this._youtubeId;
    }

    get isVideo() {
        return this._isVideo;
    }

    get isImage() {
        return this._isImage;
    }

    update(data: any) {
        this._title = data.title;
        this._date = data.date;
        this._explanation = data.explanation;
        this._url = data.url;
        this._hdurl = data.hdurl;
        this._mediaType = data.media_type;
        this._serviceVersion = data.service_version;
    }

    isEmpty() {
        return this._title === '' && this._date === '' && this._explanation === '' && this._url === '' && this._hdurl === '' && this._mediaType === '' && this._serviceVersion === '';
    }
}