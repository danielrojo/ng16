import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CalculatorService } from './services/calculator.service';
import { CalculatorComponent } from './components/calculator/calculator/calculator.component';
import { DisplayComponent } from './components/calculator/display/display.component';
import { KeyboardComponent } from './components/calculator/keyboard/keyboard.component';
import { HeroesComponent } from './components/heroes/heroes/heroes.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormComponent } from './components/heroes/form/form.component';
import { ListComponent } from './components/heroes/list/list.component';
import { ApodComponent } from './components/apod/apod/apod.component';
import { HttpClientModule } from '@angular/common/http';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { DatePickerComponent } from './components/apod/date-picker/date-picker.component';
import { ShowInfoComponent } from './components/apod/show-info/show-info.component';
import { BeersComponent } from './components/beers/beers/beers.component';
import { AbvPipe } from './pipes/abv.pipe';
import {MatSliderModule} from '@angular/material/slider';
import { TemplateComponent } from './components/forms/template/template.component';
import { ReactiveComponent } from './components/forms/reactive/reactive.component';
import { CountriesComponent } from './components/countries/countries/countries.component';
import { AppRoutingModule } from './modules/app-routing/app-routing.module';
import { PageNotFoundComponent } from './components/error/page-not-found/page-not-found.component';
import { HeroesService } from './services/heroes.service';
import { TrivialComponent } from './components/trivial/trivial/trivial.component';
import { TrivialCardComponent } from './components/trivial/trivial-card/trivial-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    FormComponent,
    ListComponent,
    ApodComponent,
    DatePickerComponent,
    ShowInfoComponent,
    BeersComponent,
    AbvPipe,
    TemplateComponent,
    ReactiveComponent,
    CountriesComponent,
    PageNotFoundComponent,
    TrivialComponent,
    TrivialCardComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    YouTubePlayerModule,
    MatSliderModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [CalculatorService, HeroesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
