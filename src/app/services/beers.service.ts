import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Beer } from '../models/beer';

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  private _beers: Beer[] = [];
  beers$ = new BehaviorSubject<Beer[]>(this._beers);


  constructor(private http: HttpClient) { }

  getBeers() {
    let observer = {
      next: (data: any) => {
        this._beers = [];
        for (const dataBeer of data) {
          this._beers.push(new Beer(dataBeer));
        }
        this.beers$.next(this._beers);
      },
      error: (error: any) => {
        console.log(error);
      },
      complete: () => {
        console.log('completed');
      }
    };
    return this.http.get('https://api.punkapi.com/v2/beers').subscribe(observer);
  }
}
