import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


enum State {
  init,
  firstNumber,
  secondNumber,
  result
}

@Injectable()
export class CalculatorService {

  public display$ = new BehaviorSubject<string>('');
  
  private currentState = State.init;
  private display = '';
  private firstFigure = 0;
  private secondFigure = 0;
  private result = 0;
  private operator = '';

  constructor() { }

  processNumber(value: number): void {
    switch (this.currentState) {
      case State.init:
        this.firstFigure = value;
        this.currentState = State.firstNumber;
        this.display = value.toString();
        break;
      case State.firstNumber:
        this.firstFigure = this.firstFigure * 10 + value;
        this.display += value.toString();
        break;
      case State.secondNumber:
        this.secondFigure = this.secondFigure * 10 + value;
        this.display += value.toString();
        break;
      case State.result:
        this.firstFigure = value;
        this.secondFigure = 0;
        this.result = 0;
        this.operator = '';
        this.currentState = State.firstNumber;
        this.display = value.toString();
        break;
    }
    this.display$.next(this.display);
  }

  processSymbol(value: string): void {
    switch (this.currentState) {
      case State.init:

        break;
      case State.firstNumber:
        if (this.isOperator(value)) {
          this.operator = value;
          this.currentState = State.secondNumber;
          this.display += value;
        }
        break;
      case State.secondNumber:
        if (value === '=') {
          try {
            this.result = this.resolve();
            this.currentState = State.result;
            this.display += value + this.result.toString();
          }
          catch (e: any) {
            this.display = e.message;
            this.initCalculator();
          }
        }
        break;
      case State.result:
        if (this.isOperator(value)) {
          this.currentState = State.secondNumber;
          this.display = this.result.toString() + value;
          this.firstFigure = this.result;
          this.operator = value;
          this.secondFigure = 0;
          this.result = 0;
        }
        break;
    }
    this.display$.next(this.display);
  }

  private isOperator(value: string) {
    return value === '+' || value === '-' || value === '*' || value === '/';
  }

  private resolve(): number {
    switch (this.operator) {
      case '+':
        return this.firstFigure + this.secondFigure;
      case '-':
        return this.firstFigure - this.secondFigure;
      case '*':
        return this.firstFigure * this.secondFigure;
      case '/':
        return this.firstFigure / this.secondFigure;
      default:
        throw new Error('Unknown operator');
    }
  }

  initCalculator() {
    this.display = '';
    this.currentState = State.init;
    this.firstFigure = 0;
    this.secondFigure = 0;
    this.result = 0;
    this.operator = '';
  }

}
