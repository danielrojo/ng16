import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  _countries: string[] = [];
  countries$ = new BehaviorSubject<string[]>(this._countries);

  constructor(private http: HttpClient) { }

  getCountries() {
    let observer = {
      next: (value: any) => {
        this._countries = value.map((country: any) => country.name.common);
        this.countries$.next(this._countries);
      },
      error: (error: any) => {
        console.log(error);
      },
      complete: () => {
        console.log('completed');
      }
    };
    return this.http.get('https://restcountries.com/v3.1/all').subscribe(observer);
  }
}
