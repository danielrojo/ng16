import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Apod } from '../models/apod';

@Injectable({
  providedIn: 'root'
})
export class ApodService {

  private data: any = {};
  private apod = new Apod();
  apod$ = new BehaviorSubject<Apod>(this.apod);

  constructor(private http: HttpClient) { }

  getApod(dateString?: string) {

    let url = 'https://api.nasa.gov/planetary/apod';
    let apiKey = 'tqz634Z1x0LiJzjbhSyUoExrZaGKLM0MG1VnROR6';
    url = `${url}?api_key=${apiKey}`;

    if (dateString !== undefined) {
      url = `${url}&date=${dateString}`;
    }

    // observer object to handle the response
    let observer = {
      next: (data: any) => {
        console.log(data.headers);
        this.data = data;
        this.apod = new Apod(data);
        this.apod$.next(this.apod);
      },
      error: (err: any) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      }
    };

    // make the request to the API and subscribe to the observer
    this.http.get(url).subscribe(observer);
  }
}
