import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Card } from '../models/card';

@Injectable({
  providedIn: 'root'
})
export class TrivialService {

  private _cards: Card[] = [];
  public cards$ = new BehaviorSubject<Card[]>(this._cards);

  constructor(private http: HttpClient) { }

  getCards() {

    let observer = {
      next: (data: any) => {
        this._cards = data.results.map((card: any) => new Card(card));
        this.cards$.next(this._cards);

      },
      error: (error: any) => {
        console.log(error);
      },
      complete: () => {
        console.log('complete');
      }
    };

    return this.http.get('https://opentdb.com/api.php?amount=10').subscribe(observer);
  }
}
