import { Injectable } from '@angular/core';
import { Hero } from '../models/hero';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class HeroesService {

  private heroes: Hero[] = [new Hero('Superman', 'Man of steel'), new Hero('Batman', 'Dark knight')];
  heroes$ = new BehaviorSubject<Hero[]>(this.heroes);

  constructor() { }

  addHero(hero: Hero) {
    if (hero.isEmpty()) {
      return;
    }
    this.heroes.push(hero);
    this.heroes$.next(this.heroes);
  }

  deleteHero(index: number) {
    if (index < 0 || index >= this.heroes.length || this.heroes.length === 0 || !Number.isInteger(index) || index === undefined || index === null || isNaN(index)) {
      return;
    }
    this.heroes.splice(index, 1);
    this.heroes$.next(this.heroes);
  }

  //clone the array
  getHeroes(): Hero[] {
    return [...this.heroes];
  }

  private getLength(): number {
    return this.heroes.length;
  }
}
