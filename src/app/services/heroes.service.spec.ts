import { TestBed } from '@angular/core/testing';

import { HeroesService } from './heroes.service';
import { Hero } from '../models/hero';

describe('HeroesService', () => {
  let service: HeroesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroesService]
    });
    service = TestBed.inject(HeroesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('I add a hero correctly', () => {
    const heroes = service.getHeroes();
    const initialLength = heroes.length;
    // add a hero
    service.addHero(new Hero('Spiderman', 'Friendly neighborhood'));
    // check that the length of the array has increased by 1
    expect(service.getHeroes().length).toBe(initialLength + 1);
  });

  it('I delete a hero correctly', () => {
    const heroes = service.getHeroes();
    const initialLength = heroes.length;
    // delete a hero
    service.deleteHero(0);
    // check that the length of the array has decreased by 1
    expect(service.getHeroes().length).toBe(initialLength - 1);
  }
  );

  it('I dont add an empty hero', () => {
    const heroes = service.getHeroes();
    const initialLength = heroes.length;
    // add an empty hero
    service.addHero(new Hero('', ''));
    // check that the length of the array has not changed
    expect(service.getHeroes().length).toBe(initialLength);
  }
  );

  it('try to remove from a worng index', () => {
    const heroes = service.getHeroes();
    const initialLength = heroes.length;
    // delete a hero from an index greater than the length of the array
    service.deleteHero(heroes.length);
    expect(service.getHeroes().length).toBe(initialLength);

    // remove a negative index
    service.deleteHero(-1);
    expect(service.getHeroes().length).toBe(initialLength);

    // remove a decimal index
    service.deleteHero(3.4);
    expect(service.getHeroes().length).toBe(initialLength);

  }
  );

  it('check that private method getLength works', () => {
    const heroes = service.getHeroes();
    const initialLength = heroes.length;
    // check that the length of the array is the same as the one returned by the private method getLength
    expect(service['getLength']()).toBe(initialLength);
  }
  );
});
