import { TestBed } from '@angular/core/testing';

import { ApodService } from './apod.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('ApodService', () => {
  let service: ApodService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient, ApodService]
    });
    service = TestBed.inject(ApodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
