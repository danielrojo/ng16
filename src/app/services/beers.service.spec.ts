import { TestBed } from '@angular/core/testing';

import { BeersService } from './beers.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('BeersService', () => {
  let service: BeersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [BeersService, HttpClient]
    });
    service = TestBed.inject(BeersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
