import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CalculatorService } from './services/calculator.service';
import { HeroesService } from './services/heroes.service';
import { Apod } from './models/apod';
import { ApodService } from './services/apod.service';
import { CountriesService } from './services/countries.service';
import { BeersService } from './services/beers.service';
import { AppRoutingModule } from './modules/app-routing/app-routing.module';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [AppComponent],
    imports: [AppRoutingModule],
    providers: [CalculatorService, HeroesService, ApodService, CountriesService, BeersService]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeDefined();
  });

  it(`should have as title 'ng16'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('ng16');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    // check that <a class="navbar-brand" href="#">Angular</a> has been rendered

    expect(compiled.querySelector('a.navbar-brand')?.textContent).toContain('Angular');
  });
});
