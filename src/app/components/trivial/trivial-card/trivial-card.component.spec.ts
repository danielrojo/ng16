import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrivialCardComponent } from './trivial-card.component';

describe('TrivialCardComponent', () => {
  let component: TrivialCardComponent;
  let fixture: ComponentFixture<TrivialCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TrivialCardComponent]
    });
    fixture = TestBed.createComponent(TrivialCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
