import { Component, Input } from '@angular/core';
import { Card } from 'src/app/models/card';

@Component({
  selector: 'app-trivial-card',
  templateUrl: './trivial-card.component.html',
  styleUrls: ['./trivial-card.component.scss']
})
export class TrivialCardComponent {

  @Input() card: Card = new Card();

  buttonClass = [
    'btn btn-primary',
    'btn btn-primary',
    'btn btn-primary',
    'btn btn-primary'
  ]

  constructor() { }

  handleAnswer(answer: string, index: number) {
    this.card.isResponded = true;
    this.card.rightAnswered = answer === this.card.correctAnswer;
    this.buttonClass[index] = this.card.rightAnswered ? 'btn btn-success' : 'btn btn-danger';
  }


}
