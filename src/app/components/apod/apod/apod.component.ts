import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Apod } from 'src/app/models/apod';
import { ApodService } from 'src/app/services/apod.service';
// import momentjs
import * as moment from 'moment';


@Component({
  selector: 'app-apod',
  templateUrl: './apod.component.html',
  styleUrls: ['./apod.component.scss']
})
export class ApodComponent implements OnInit{

  dateStr = moment().format('YYYY-MM-DD');

  constructor() { }

  ngOnInit(): void {

  }

  dateChange(value: any) {
    console.log('dateChange: ', value);
    this.dateStr = value;
  }
    
}
