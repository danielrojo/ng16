import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
import { Apod } from 'src/app/models/apod';
import { ApodService } from 'src/app/services/apod.service';

@Component({
  selector: 'app-show-info',
  templateUrl: './show-info.component.html',
  styleUrls: ['./show-info.component.scss']
})
export class ShowInfoComponent implements OnInit, OnChanges {
  
  @Input() date = moment().format('YYYY-MM-DD');

  apod = new Apod();

  apiLoaded = false;


  constructor(private service: ApodService) { }


  ngOnInit(): void {
        // load the youtube iframe api
    if (!this.apiLoaded) {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }

    // subscribe to the apod$ observable
    this.service.apod$.subscribe(
      value => {
        this.apod = value
      }
    );

    // get the apod of today
    this.service.getApod();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes: ', changes);
    if (changes['date'].currentValue !== changes['date'].previousValue && changes['date'].currentValue !== undefined) {
      this.service.getApod(changes['date'].currentValue);
    }
  }

}
