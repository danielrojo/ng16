import { Component, EventEmitter, Output } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent {

  @Output() onDateChange = new EventEmitter<string>();

  model!: NgbDateStruct;

  dateSelected(value: any) {
    let date = moment(value.year + '-' + value.month + '-' + value.day).format('YYYY-MM-DD');
    this.onDateChange.emit(date);
  }
}
