import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent {

  @Output() onClick = new EventEmitter<string | number>();

  doOnClick(value: string | number) {
    this.onClick.emit(value);
  }

  initCalculator() {
    this.onClick.emit('init');
  }

}
