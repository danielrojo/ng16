import { Component, OnInit } from '@angular/core';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  display = '';

  constructor(private service: CalculatorService) {
    
   }

  ngOnInit(): void {
    this.service.display$.subscribe(
      value => this.display = value
    );
  }

  processEvent(value: string | number) {
    if (typeof value === 'number') {
      this.service.processNumber(value);
    } else if (typeof value === 'string') {
      value === 'init' ? this.service.initCalculator() : this.service.processSymbol(value);
    }
  }


}
