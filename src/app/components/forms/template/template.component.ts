import { Component } from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent {

  namePattern = '.{2,5}[a-zA-ZñÑáéíóúÁÉÍÓÚ]';

  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];

  model = {id:18, name:'Dr IQ', power:this.powers[0],  alterEgo:'Chuck Overstreet'};

  submitted = false;

  onSubmit() { 
    this.submitted = true; 
    console.log(this.model);
    
  }

  newHero() {
    this.model = {id:0, name:'', power:'', alterEgo:''};
  }

}
