import { Component } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss']
})
export class ReactiveComponent {
  profileForm = this.fb.group({
    firstName: ['', Validators.pattern('[a-zA-Z ]*')],
    lastName: [''],
    address: this.fb.group({
        street: [''],
        city: [''],
        state: [''],
        zip: ['']
    }),
    aliases: this.fb.array([
        this.fb.control('')
    ])
});

get aliases() {
  return this.profileForm.get('aliases') as FormArray;
}

constructor(private fb: FormBuilder) { }

updateProfile() {
  this.profileForm.patchValue({
      firstName: 'Nancy',
      address: {
          street: '123 Drew Street'
      }
  });
}
addAlias() {
  this.aliases.push(this.fb.control(''));
}
onSubmit() {
  // TODO: Use EventEmitter with form value
  console.warn(this.profileForm.value);
}

}
