import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeersComponent } from './beers.component';
import { BeersService } from 'src/app/services/beers.service';
import { MatSliderModule } from '@angular/material/slider';
import { HttpClientModule } from '@angular/common/http';

xdescribe('BeersComponent', () => {
  let component: BeersComponent;
  let fixture: ComponentFixture<BeersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BeersComponent],
      imports: [MatSliderModule, HttpClientModule],
      providers: [BeersService]
    });
    fixture = TestBed.createComponent(BeersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
