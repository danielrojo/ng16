import { Component, OnInit } from '@angular/core';
import { filter, of } from 'rxjs';
import { Beer } from 'src/app/models/beer';
import { BeersService } from 'src/app/services/beers.service';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit{

  beers: Beer[] = [];
  showBeers: Beer[] = [];

  range = [2,5];

  constructor(private beersService: BeersService) { }

  ngOnInit(): void {
    this.beersService.beers$.subscribe(
      (data: any) => {
        this.beers = data;
        this.filter();
      }
    );
    this.beersService.getBeers();
  }

  changeLowRange(value: any) {
    this.range[0] = value;
    this.filter();
  }

  changeHighRange(value: any) {
    this.range[1] = value;
    this.filter();
  }

  filter () {
    console.log(`filtering beers with range ${this.range[0]} - ${this.range[1]}`);
    
    this.showBeers = this.beers.filter(
      (beer: Beer) => {
        return beer.abv >= this.range[0] && beer.abv <= this.range[1];
      }
    ).sort(
      (beerA: Beer, beerB: Beer) => {
        return beerA.abv - beerB.abv;
      }
    );

  }


}
