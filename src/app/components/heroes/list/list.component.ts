import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Hero } from 'src/app/models/hero';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  @Input() heroesList: Hero[] = [];
  @Output() onDeleteHero = new EventEmitter<number>();

  constructor() { }

  deleteHero(index: number) {
    this.onDeleteHero.emit(index);
  }
}
