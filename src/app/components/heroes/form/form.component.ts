import { Component, EventEmitter, Output } from '@angular/core';
import { Hero } from 'src/app/models/hero';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {

  @Output() onAddHero = new EventEmitter<Hero>();

  name = '';
  description = '';

  addHero() {
      this.onAddHero.emit(new Hero(this.name, this.description));
      this.name = '';
      this.description = '';
  }
}
