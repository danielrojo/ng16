import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesComponent } from './heroes.component';
import { FormsModule } from '@angular/forms';
import { HeroesService } from 'src/app/services/heroes.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormComponent } from '../form/form.component';
import { ListComponent } from '../list/list.component';

describe('HeroesComponent', () => {
  let component: HeroesComponent;
  let fixture: ComponentFixture<HeroesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeroesComponent, FormComponent, ListComponent],
      imports: [FormsModule, NgbModule],
      providers: [HeroesService]
    });
    fixture = TestBed.createComponent(HeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
