import { Component, OnDestroy, OnInit } from '@angular/core';
import { Hero } from 'src/app/models/hero';
import { HeroesService } from 'src/app/services/heroes.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit, OnDestroy{

  heroes: Hero[] = [];

  constructor(private service:HeroesService) {
    
  }

  ngOnInit(): void {
    this.service.heroes$.subscribe(
      value => this.heroes = [...value]
    );
  }

  ngOnDestroy(): void {
    console.log('HeroesComponent destroyed');
    
  }

  addHero(hero: Hero) {
      this.service.addHero(hero);
  }

  deleteHero(index: number) {
    this.service.deleteHero(index);
  }

}
