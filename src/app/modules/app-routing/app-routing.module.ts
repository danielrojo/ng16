import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from 'src/app/components/heroes/heroes/heroes.component';
import { ApodComponent } from 'src/app/components/apod/apod/apod.component';
import { BeersComponent } from 'src/app/components/beers/beers/beers.component';
import { CountriesComponent } from 'src/app/components/countries/countries/countries.component';
import { ReactiveComponent } from 'src/app/components/forms/reactive/reactive.component';
import { TemplateComponent } from 'src/app/components/forms/template/template.component';
import { PageNotFoundComponent } from 'src/app/components/error/page-not-found/page-not-found.component';
import { sampleGuard } from 'src/app/guards/sample.guard';
import { TrivialComponent } from 'src/app/components/trivial/trivial/trivial.component';



const routes: Routes = [
  { path: 'calculator', loadChildren: () => import('../calculator/calculator.module').then(m => m.CalculatorModule) }, 
  { path: 'heroes', component: HeroesComponent, canActivate: [sampleGuard] },
  { path: 'apod', component: ApodComponent },
  { path: 'beers', component: BeersComponent },
  { path: 'template_forms', component: TemplateComponent },
  { path: 'reactive_forms', component: ReactiveComponent },
  { path: 'countries', component: CountriesComponent },
  { path: 'trivial', component: TrivialComponent },
  { path: '', redirectTo: '/trivial', pathMatch: 'full'},
 
  { path: '**', component: PageNotFoundComponent, pathMatch: 'full'}
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
