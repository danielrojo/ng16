var BTNode = /** @class */ (function () {
    function BTNode(_data, _id, _left, _right) {
        if (_id === void 0) { _id = 0; }
        if (_left === void 0) { _left = null; }
        if (_right === void 0) { _right = null; }
        this._data = _data;
        this._id = _id;
        this._left = _left;
        this._right = _right;
    }
    Object.defineProperty(BTNode.prototype, "data", {
        get: function () {
            return this._data;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BTNode.prototype, "left", {
        get: function () {
            console.log('get left');
            return this._left;
        },
        set: function (node) {
            console.log('set left: ' + node.toString());
            this._left = node;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BTNode.prototype, "right", {
        get: function () {
            return this._right;
        },
        set: function (node) {
            this._right = node;
        },
        enumerable: false,
        configurable: true
    });
    BTNode.prototype.toString = function () {
        return "".concat(this.data, ": ").concat(this.left, " | ").concat(this.right);
    };
    return BTNode;
}());
var Btree = /** @class */ (function () {
    function Btree(root) {
        if (root === void 0) { root = null; }
        this.root = root;
    }
    return Btree;
}());
var node1 = new BTNode(1);
var node2 = new BTNode({ name: 'John' });
var node3 = new BTNode(3);
node1.left = node2;
node1.right = node3;
var tree = new Btree(node1);
