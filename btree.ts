class BTNode<T> {
  constructor(private _data: T,  private _id = 0, private _left: BTNode<T> = null, private _right: BTNode<T> = null) {
  }

    get data(): T {
        return this._data;
    }

    get left(): BTNode<T> {
        console.log('get left');
        
        return this._left;
    }

    set left(node: BTNode<T>) {
        console.log('set left: ' + node.toString());
        this._left = node;
    }

    get right(): BTNode<T> {
        return this._right;
    }

    set right(node: BTNode<T>) {
        this._right = node;
    }

    toString(): string {
        return `${this.data}: ${this.left} | ${this.right}`;
    }
}

class Btree {
    constructor(public root: BTNode<number> = null) {}
}

let node1: BTNode<any> = new BTNode(1);
let node2: BTNode<any> = new BTNode({name: 'John'});
let node3: BTNode<any> = new BTNode(3);
node1.left = node2;
node1.right = node3;
const tree = new Btree(node1);